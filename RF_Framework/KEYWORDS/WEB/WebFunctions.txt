*** Settings ***
Resource          ../../SETTINGS/StandardLibrary.txt
Resource          ../../SETTINGS/AllResource.txt

*** Keywords ***
[W] Capture Page Screenshot
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Capture Page Screenshot
    ${DateTime}    Get Current Date    result_format=%Y%m%d%H%M%S%f
    Comment    ${ScreenshotDir}    Convert Date    ${DateTime}    result_format=%Y.%m.%d
    Comment    ${ScreenshotDir}    Set Variable    SCREENSHOT${/}${ScreenshotDir}${/}${TEST_NAME}
    Comment    ${Dir}    Replace String    ${CURDIR}    FRAMEWORKS    ${ScreenshotDir}
    Log To Console    Capture Dir --> ${newResultFolder}    ${newResultFolder}
    ${OutputScreenshot}    Set Variable    ${newResultFolder}${/}${testCaseStep}#${runningStep}_${KEYWORD_NAME}_${DateTime}.PNG
    Sleep    3s
    Capture Page Screenshot    ${OutputScreenshot}
    Comment    KEYWORD STATUS END
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${OutputScreenshot}

[W] Check Element
    [Arguments]    ${stepName}    ${ElementCheck}    ${timeout}=10s
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Wait Until Page Contains Element    ${ElementCheck}    timeout=${timeout}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Check Message
    [Arguments]    ${stepName}    ${Value}    ${timeout}=10s
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Wait Until Page Contains    ${Value}    timeout=${timeout}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Click Element
    [Arguments]    ${stepName}    ${ElementClick}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${ElementClick}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Click Element Position
    [Arguments]    ${stepName}    ${ElementClick}    ${Position}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Sessions}    Get WebElements    ${ElementClick}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    @{Sessions}[${Position}]
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Double Click Element
    [Arguments]    ${stepName}    ${ElementDoubleClick}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementDoubleClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Double Click Element    ${ElementDoubleClick}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Double Click Element Position
    [Arguments]    ${stepName}    ${ElementDoubleClick}    ${Position}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Sessions}    Get WebElements    ${ElementDoubleClick}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Double Click Element    @{Sessions}[${Position}]
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Enter
    [Arguments]    ${stepName}    ${ElementEnter}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementEnter}
    Press Key    ${ElementEnter}    \\13
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Get Text
    [Arguments]    ${ElementGetText}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Text
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetText}
    ${Return}    Get Text    ${ElementGetText}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${Return}

[W] Get Text Position
    [Arguments]    ${ElementGetText}    ${Position}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Text Position
    ${Sessions}    Get WebElements    ${ElementGetText}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Return}    Get Text    @{Sessions}[${Position}]
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${Return}

[W] Get Value
    [Arguments]    ${ElementGetValue}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Value
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementGetValue}
    ${Return}    Get Value    ${ElementGetValue}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${Return}

[W] Get Value Position
    [Arguments]    ${ElementGetValue}    ${Position}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Get Value Position
    ${Sessions}    Get WebElements    ${ElementGetValue}
    Insert Into List    ${Sessions}    0    ${EMPTY}
    ${Return}    Get Value    @{Sessions}[${Position}]
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN
    [Return]    ${Return}

[W] Go Back
    [Arguments]    ${stepName}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Go Back
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Input Password
    [Arguments]    ${stepName}    ${ElementInputPassword}    ${Value}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputPassword}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Password    ${ElementInputPassword}    ${Value}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Input Text
    [Arguments]    ${stepName}    ${ElementInputText}    ${Value}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    ${ElementInputText}    ${Value}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Input Manual
    [Arguments]    ${stepName}    ${ElementInputText}    ${Expected}
    Set Library Search Order    Selenium2Library
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${ManualInput}    Get Value From User    Input value
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    ${ElementInputText}    ${ManualInput}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Mouse Over
    [Arguments]    ${ElementMouseOver}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Mouse Over
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementMouseOver}
    Mouse Over    ${ElementMouseOver}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Open Browser
    [Arguments]    ${stepName}    ${URL}    ${Browser}=Chrome
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Open Browser    ${URL}    ${Browser}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    FOR    ${i}    IN    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Maximize Browser Window
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    END
    Log To Console    ${stepName} Status : ${Status}
    Log To Console    ${stepName} Error : ${Error}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Select Frame
    [Arguments]    ${stepName}    @{Element}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    Unselect Frame
    FOR    ${i}    IN    @{Element}
        WAIT FOR PAGE CONTAINS ELEMENT    ${i}
        Select Frame    ${i}
    END
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Select From List By Value
    [Arguments]    ${stepName}    ${Element}    ${Value}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${Element}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Select From List By Label    ${Element}    ${Value}
    Run Keyword If    "${Status}" != "PASS"    Select From List By Value    ${Element}    ${Value}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Result Get Text --> ${Value}
    FOR    ${i}    IN    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Should Be Equal As Strings    "${Value}"    "${Expected}"
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    "${Status}" != "PASS"    [W] Replace Error Message    ${Status}    ${Value}    ${Expected}
    END
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Close Browser
    [Arguments]    ${stepName}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Close All Browsers
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Switch To Window
    [Arguments]    ${TAB}=1
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    [W] Switch To Window
    ${Windows}    Get Window Handles
    Insert Into List    ${Windows}    0    ${EMPTY}
    FOR    ${i}    IN    @{Windows}[${TAB}]
        Switch Window    ${i}
    END
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Verify Result
    [Arguments]    ${stepName}    ${Value}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    Log To Console    Value --> ${Value}
    Log To Console    Expected --> ${Expected}
    Log To Console    Value --> ${Value}
    FOR    ${i}    IN    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Should Be Equal As Strings    "${Value}"    "${Expected}"
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    "${Status}" != "PASS"    [W] Replace Error Message    ${Status}    ${Value}    ${Expected}
    END
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Mouse Down
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Comment    KEYWORD STATUS PENDING    [W] Mouse Over
    WAIT FOR PAGE CONTAINS ELEMENT    ${Element}
    Mouse Down    ${Element}
    Comment    KEYWORD STATUS END    None
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Click Element and Verify Result
    [Arguments]    ${stepName}    ${ElementClick}    ${ElementGetText}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementClick}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Click Element    ${ElementClick}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    ${result}    [W] Get Text    ${ElementGetText}
    Log To Console    ${stepName} Result Get Text --> ${result}
    FOR    ${i}    IN    1    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Should Be Equal As Strings    "${result}"    "${Expected}"
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    "${Status}" != "PASS"    [W] Replace Error Message    ${Status}    ${result}    ${Expected}
    END
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Input Text and Verify Result
    [Arguments]    ${stepName}    ${ElementInputText}    ${ElementGetValue}    ${Value}    ${Expected}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    ${stepName}
    WAIT FOR PAGE CONTAINS ELEMENT    ${ElementInputText}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Input Text    ${ElementInputText}    ${Value}
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
    ${result}    [W] Get Value    ${ElementGetValue}
    Log To Console    ${stepName} Result Get Text --> ${result}
    Comment    ${result}    Replace String    ${result}    ,    ${EMPTY}
    Comment    ${Expected}    Replace String    ${Expected}    ,    ${EMPTY}
    FOR    ${i}    IN    1    1
        ${Status}    ${Error}    Run Keyword And Ignore Error    Should Be Equal As Strings    "${result}"    "${Expected}"
        Set Suite Variable    ${STEP_STATUS}    ${Status}
        Set Suite Variable    ${ERROR_DESCRIPTION}    ${Error}
        Run Keyword If    "${Status}" != "PASS"    [W] Replace Error Message    ${Status}    ${result}    ${Expected}
    END
    Log To Console    ${stepName} Status : ${STEP_STATUS}
    Log To Console    ${stepName} Error : ${ERROR_DESCRIPTION}
    ${ScreenShot}    [W] Capture Page Screenshot
    KEYWORD STATUS END    ${ScreenShot}
    [Teardown]    KEYWORD STATUS TEARDOWN

[W] Replace Error Message
    [Arguments]    ${Status}    ${Value}    ${Expected}
    Log To Console    Replace Error Message
    Set Suite Variable    ${STEP_STATUS}    ${Status}
    Set Suite Variable    ${ERROR_DESCRIPTION}    ข้อมูลที่ส่งเข้ามา ${Value} ไม่ตรงกับ ${Expected}
